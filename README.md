# Popotamo Profiles

Ce dépôt contient les profils des joueurs du Popotamo d'origine, avant que Motion Twin ne ferme définitivement le site. 
- Joueurs de niveau 4 ("Champion")
- Joueurs de niveau 3 ("Avancé")
- Joueurs de niveau 2 ("Intermédiaire")

Les joueurs de niveau 1 ("Débutant") ne sont PAS sauvegardés car extrêmement nombreux et peu d'intérêt de préserver des profils ayant peu évolué.


Intérêt de la sauvegarde :

=> Permet de sauvegarder les profils de ceux qui ont joué pendant de nombreuses années au jeu MT  

=> Les pages étant dans un HTML simple, elles pourront éventuellement être parsées pour transférer les données proprement dans une base de données.

